var canvas = document.getElementById("myCanvas");
var context = canvas.getContext("2d");
// blue square
//let adiv = document.getElementById('mydiv');
//let leftpos = 7;
// Zombie image setup
const scale = 1;
const width = 56;
const height = 72;
const scaledWidth = scale * width;
const scaledHeight = scale * height;
const walkLoop = [1,0,1,2];
const frameLimit = 7;
let zombie_image = new Image();
zombie_image.src = "assets/image/zombie-sprite-sheet.png";
// random position image setup
const brainScale = 1;
const brainWidth = 50;
const brainHeight = 44;
const scaledBrainWidth = brainScale * brainWidth;
const scaledBrainHeight = brainScale * brainHeight;
let brain_image = new Image();
brain_image.src = "assets/image/brain.png";
// animation
let currentLoopIndex = 0;
let frameCount = 0;
let currentDirection = 0;
let speed = 5.0;
// health bar
var healthBarWidth = 100;
var healthBarHeight = 20;
var healthBarMax = 100;
var healthBarVal = 0;
// score
var score = 0;

/*
function movediv(){
    //console.log(timestamp);
    //printLegible(timestamp)
    leftpos += 0.2;
    adiv.style.left = leftpos + 'px';
    if (leftpos < canvas.width - 40){
        window.requestAnimationFrame(movediv); // call requestAnimationFrame again to animate next frame
    }
    // context.drawImage(adiv, leftpos, 0);
}
*/

function GameObject(sprite, xpos, ypos, width, height) {
    this.sprite = sprite;
    this.xpos = xpos;
    this.ypos = ypos;
    this.width = width;
    this.height = height;
}
// default player
let zombie_player = new GameObject(zombie_image, 0, 0, scaledWidth, scaledHeight);
// default brain
let brain_randPos = new GameObject(brain_image, 100, 100, scaledBrainWidth, scaledBrainHeight);

// input function
function GamerInput(input) {
    this.action = input;
}

let gamerInput = new GamerInput("None");

function input(event) 
{
    if (event.type === "keydown") 
    {
        switch (event.keyCode) 
        {
            case 37: // Left Arrow
                gamerInput = new GamerInput("Left");
                break;
            case 38: // Up Arrow
                gamerInput = new GamerInput("Up");
                break;
            case 39: // Right Arrow
                gamerInput = new GamerInput("Right");
                break;
            case 40: // Down Arrow
                gamerInput = new GamerInput("Down");
                break;
            default:
                gamerInput = new GamerInput("None"); //No Input
        }
    } 
    /*
    else if (event.type === "keyup")
    {
        switch (event.keyCode)
        {
            case 32: // space bar
                gamerInput = new GamerInput("Space");
                break;
            default:
                gamerInput = new GamerInput("None");
        }
    }
    */
    else if (event.type === "mousedown" || event.type === "touchstart")
    {
        console.log(event);
        if (event.target.className === "button W round")
        {
            gamerInput = new GamerInput("Up");
            //console.log("clicked up");
        }
        else if (event.target.className === "button S round")
        {
            gamerInput = new GamerInput("Down");
        }
        else if (event.target.className === "button A round")
        {
            gamerInput = new GamerInput("Left");
        }
        else if (event.target.className === "button D round")
        {
            gamerInput = new GamerInput("Right");
        }
    }
    else 
    {
        gamerInput = new GamerInput("None");
    }
    //healthBarVal += 10;
}

// Draw a HealthBar on Canvas, can be used to indicate players health
function drawHealthbar() 
{
    // Draw the background
    context.fillStyle = "#000000";
   // context.clearRect(0, 0, canvas.width, canvas.height);
    context.fillRect(zombie_player.xpos - ((healthBarWidth - scaledBrainWidth) / 2.0), zombie_player.ypos - healthBarHeight, healthBarWidth, healthBarHeight);
  
    // Draw the fill
    context.fillStyle = "#00FF00";
    var fillVal = Math.min(Math.max(healthBarVal / healthBarMax, 0), 1);
    context.fillRect(zombie_player.xpos - ((healthBarWidth - scaledBrainWidth) / 2.0), zombie_player.ypos - healthBarHeight, fillVal * healthBarWidth, healthBarHeight);
}

// nipplejs setup
var dynamic = nipplejs.create({
    zone: document.getElementById('joystick'),
    color: 'grey',
});

dynamic.on('added', function (evt, nipple){
    nipple.on('dir:up', function (evt, data){
        gamerInput = new GamerInput("Up");
    });
    nipple.on('dir:down', function (evt, data){
        gamerInput = new GamerInput("Down");
    });
    nipple.on('dir:left', function (evt, data){
        gamerInput = new GamerInput("Left");
    });
    nipple.on('dir:right', function (evt, data){
        gamerInput = new GamerInput("Right");
    });
    nipple.on('end', function (evt, data){
        gamerInput = new GamerInput("None");
    });
});

// Spritesheet atlas references
// row 0 up
// row 1 down
// row 2 right
// row 3 left

function boundCollision()
{
    if (zombie_player.xpos <= 0) // collide with left edge
    {
        zombie_player.xpos = 0;
    }
    else if (zombie_player.xpos >= canvas.width - width) // collide with right edge
    {
        zombie_player.xpos = canvas.width - width
    }
    if (zombie_player.ypos <= 0) // collide with top edge
    {
        zombie_player.ypos = 0;
    }
    else if (zombie_player.ypos >= canvas.height - height) // collide with bottom edge
    {
        zombie_player.ypos = canvas.height - height;
    }
}

function update() 
{
    if (gamerInput.action === "Up") 
    {
        //console.log("Move Up");
        zombie_player.ypos -= speed; // Move Player Up
        currentDirection = 0;
    } 
    else if (gamerInput.action === "Down") 
    {
        //console.log("Move Down");
        zombie_player.ypos += speed; // Move Player Down
        currentDirection = 1;
    } 
    else if (gamerInput.action === "Left") 
    {
        //console.log("Move Left");
        zombie_player.xpos -= speed; // Move Player Left
        currentDirection = 3;
    } 
    else if (gamerInput.action === "Right") 
    {
        //console.log("Move Right");
        zombie_player.xpos += speed; // Move Player Right
        currentDirection = 2;
    }
    /*
    else if (gamerInput.action === "Space")
    {
        //console.log("Reset position");
        haotai_Player.xpos = 0;
        haotai_Player.ypos = 0;
    }
    */
    //window.requestAnimationFrame(checkDistanceBetweenHaotaiAndSquare);
    boundCollision();
}

/*
function draw() {
    context.clearRect(0, 0, canvas.width, canvas.height);
    // console.log(haotai_Player);
    context.drawImage(haotai_Player.sprite, haotai_Player.xpos, haotai_Player.ypos, haotai_Player.width, haotai_Player.height);
    // context.fillRect(haotai_Player.xpos+100, haotai_Player.ypos+100, 50, 50);
}
*/

// draw randPos Brain
function generateRandPos(maxX, maxY, delta)
{
    this.x = Math.abs(Math.floor(Math.random() * maxX) + delta);
    this.y = Math.abs(Math.floor(Math.random() * maxY) + delta);
}

function drawBrain()
{
    context.drawImage(brain_randPos.sprite, brain_randPos.xpos, brain_randPos.ypos);
    // check collision
    /* if (zombie_player.xpos < brain_randPos.xpos + brain_randPos.width
        && zombie_player.xpos + zombie_player.width > brain_randPos.xpos
        && zombie_player.ypos < brain_randPos.ypos
        && zombie_player.ypos + zombie_player.height > brain_randPos.ypos) */
    if (zombie_player.xpos - brain_randPos.xpos >= zombie_player.width * (-1) && zombie_player.xpos - brain_randPos.xpos <= brain_randPos.width)
    // xpos difference is between minus player width and positive item width
    {
        if (zombie_player.ypos - brain_randPos.ypos >= zombie_player.height * (-1) && zombie_player.ypos - brain_randPos.ypos <= brain_randPos.height)
        // ypos difference is between minus player height and positive item height
        {
            var randPos = new generateRandPos(600 - scaledBrainWidth, 300 - scaledBrainHeight, 200);
            brain_randPos.xpos = randPos.x;
            brain_randPos.ypos = randPos.y;
            healthBarVal += 5;
            score += 1;
        }
    }  
}

function drawFrame(image, frameX, frameY, canvasX, canvasY)
{
    context.drawImage(image, frameX * width, frameY * height, width, height,
                    canvasX, canvasY, scaledWidth, scaledHeight);
}

function animate()
{
    if (gamerInput.action != "None")
    {
        frameCount++;
        console.log(currentLoopIndex);
        if (frameCount >= frameLimit)
        {
            frameCount = 0;
            currentLoopIndex++;
            if (currentLoopIndex == walkLoop.length)
            {
                currentLoopIndex = 0;
            }
        }
    }
    else
    {
        currentLoopIndex = 0;
    }
    drawFrame(zombie_player.sprite, walkLoop[currentLoopIndex], currentDirection, 
        zombie_player.xpos, zombie_player.ypos);
}

function scoreCounter()
{
    context.font = "bold 15px solid";
    var scoreCounterString = "Score: " + score;
    context.fillStyle = "#000000";
    context.fillText(scoreCounterString, 0, canvas.height);
}

function draw()
{
    context.clearRect(0,0, canvas.width, canvas.height);
    animate();
    drawHealthbar();
    drawBrain();
    scoreCounter();
    //context.drawImage(brain_randPos.sprite, brain_randPos.xpos, brain_randPos.ypos);
}

function gameloop() 
{
    update();
    draw();
    window.requestAnimationFrame(gameloop);
}
/*
function printLegible(timestamp){
    let date = new Date(timestamp);
    console.log("Date: "+date.getDate()+
        "/"+(date.getMonth()+1)+
        "/"+date.getFullYear()+
        " "+date.getHours()+
        ":"+date.getMinutes()+
        ":"+date.getSeconds());
}
*/
/*
function currentTime()
{
    let date_2 = new Date();
    var hour = date_2.getHours();
    var min = date_2.getMinutes();
    var sec = date_2.getSeconds();
    var day = date_2.getDate();
    var month = date_2.getMonth();
    var year = date_2.getFullYear();
    hour = updateTime(hour);
    min = updateTime(min);
    sec = updateTime(sec);
    document.getElementById("clock").innerHTML = "Year " + year + " Month " + (month+1) + " Day " + day + " " + hour + ":" + min + ":" + sec;
    var x = setTimeout(function(){currentTime()}, 1000);
}

function updateTime(e)
{
    if (e < 10)
    {
        return "0" + e;
    }
    else
    {
        return e;
    }
}
*/
// currentTime();
window.addEventListener('keydown', input);
window.addEventListener('keyup', input);
window.addEventListener('mousedown', input);
window.addEventListener('mouseup', input);
window.addEventListener('touchstart', input);
window.addEventListener('touchend', input);

//window.requestAnimationFrame(checkDistanceBetweenHaotaiAndSquare);
window.requestAnimationFrame(gameloop);
//window.requestAnimationFrame(currentTime);
//window.requestAnimationFrame(movediv); // call requestAnimationFrame and pass into it animation function